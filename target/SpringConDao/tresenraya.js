var self;
function ViewModel() {
	self = this;
	
	self.usuarios = ko.observableArray([]);
	self.mensaje = ko.observable("");
	self.mensajeErr = ko.observable("");
	self.tablero = ko.observableArray([]);
	self.msgFicha = ko.observable("");
	
	var idMatch = sessionStorage.idMatch;
	var started = JSON.parse(sessionStorage.started);
	var turno; // Se le agrega el valor en onmessage
	var ficha; // Se le agrega el valor en onmessage
	var miSession; // Se le agrega el valor en onmessage
	
	var url = "ws://localhost:8600/juegos";
	self.ws = new WebSocket(url);
	
	if (started) {
		self.mensaje("La partida ha comenzado");
	} else {
		self.mensaje("Esperando oponente para la partida " + idMatch);
	}

	self.ws.onopen = function(event) {
		var msg = {
			type : "ready",
			idMatch : sessionStorage.idMatch
		};
		self.ws.send(JSON.stringify(msg));
	}

	self.ws.onmessage = function(event) {
		var data = event.data;
		data = JSON.parse(data);
		
		if (data.type == "matchStarted") {
			turno = data.startData.turno;
			ficha = data.startData.ficha;
			miSession = data.startData.miSession;
			self.msgFicha("Tu ficha es la número: "+ficha);
			self.mensaje("La partida ha comenzado");
			var players = data.players;
			for (var i=0; i<players.length; i++) {
				self.usuarios.push(players[i].userName);
			}
//			var tablero = data.startData.tablero;
			for (var f=0; f<9; f++){
				var fichaNueva = new Ficha(ko, 0, f);
				self.tablero.push(fichaNueva);
			}
			if(miSession == turno)
				self.mensajeErr("Es tu turno");
			else
				self.mensajeErr("Es el turno del rival");
			
		} else if (data.type == "actualizacionTablero") {
			turno = data.turno;
			if(miSession == turno)
				self.mensajeErr("Es tu turno");
			else
				self.mensajeErr("Es el turno del rival");
			var posicionActualizada = data.posicionActualizada;
			self.tablero()[posicionActualizada].valor(data.fichaPuesta);
			
			if(data.empate){
				alert(data.empate);
				window.location.href = "games.html";
			}
			
		} else if (data.type == "error"){
			self.mensajeErr(data.mensaje);
		} else if (data.type == "ganador"){
			var posicionFinal = data.fichaFinal;
			self.tablero()[posicionFinal].valor(data.valorFinal);
			alert("El ganador de esta partida es: "+ data.ganador +"!");
			window.location.href = "games.html";
		}
	}
}

class Ficha {
	constructor(ko, valor, posicion) {
		this.valor = ko.observable(valor);
		this.posicion = posicion;
	}
	
	mover() {
		var msg = {
			type : "jugada",
			idMatch : sessionStorage.idMatch,
			posicion : this.posicion
		};
		self.ws.send(JSON.stringify(msg));
	}
}

var vm = new ViewModel();
ko.applyBindings(vm);