var self;
function ViewModel() {
	self = this;
	
	self.usuarios = ko.observableArray([]);
	self.mensaje = ko.observable("");
	self.mensajeErr = ko.observable("");
	self.fichaIzdaEnMesa = ko.observable(new Ficha(-1, -1));
	self.fichaDchaEnMesa = ko.observable(new Ficha(-1, -1));
	self.fichaSeleccionada = ko.observable(new Ficha(-1, -1));
	self.fichasJugador = ko.observableArray([]);
	
	var idMatch = sessionStorage.idMatch;
	var started = JSON.parse(sessionStorage.started);
	var turno; // Se le agrega el valor en onmessage
	var miSession; // Se le agrega el valor en onmessage
	
	self.nfichas = ko.observable(-1);
	
	var url = "ws://localhost:8600/juegos";
	self.ws = new WebSocket(url);
	
	if (started) {
		self.mensaje("La partida ha comenzado");
	} else {
		self.mensaje("Esperando oponente para la partida " + idMatch);
	}

	self.ws.onopen = function(event) {
		var msg = {
			type : "ready",
			idMatch : sessionStorage.idMatch
		};
		self.ws.send(JSON.stringify(msg));
	}

	self.ws.onmessage = function(event) {
		var data = event.data;
		data = JSON.parse(data);
		
		console.log(data);
		
		if (data.type == "matchStarted") {
			
			turno = data.startData.turno;
			miSession = data.startData.miSession;
			
			self.mensaje("La partida ha comenzado");
			
			var players = data.players;
			for (var i=0; i<players.length; i++) {
				self.usuarios.push(players[i].userName);
			}
			
			var fichas = data.startData.data;
			for (var c=0; c<fichas.length;c++){
				self.fichasJugador.push(new Ficha(fichas[c].left, fichas[c].right));
			} 
			if(miSession == turno){
				self.mensajeErr("Es tu turno");
				if(self.fichaDchaEnMesa().left == -1 && self.fichaIzdaEnMesa().left == -1){
					var msg = {
							type : "jugada",
							idMatch : sessionStorage.idMatch,
							subType : "asignacionTurno"
						};
					self.ws.send(JSON.stringify(msg));
				}
			}else{
				self.mensajeErr("Es el turno del rival");
			}
		} else if (data.type == "actualizacionTablero") {
			
			self.fichaSeleccionada(new Ficha(-1, -1));			
			
			turno = data.turno;
			if(miSession == turno){
				self.mensajeErr("Es tu turno");
				if(data.mejorDoble){
					for (var b=0; b<self.fichasJugador().length; b++){ 
						if (self.fichasJugador()[b].left == data.mejorDoble) {
							self.fichasJugador.splice(b, 1);
							var msg1 = {
									type : "jugada",
									idMatch : sessionStorage.idMatch,
									left : data.mejorDoble,
									right : data.mejorDoble,
									leftMesa : -1,
									rightMesa : -1,
									pos : "izquierda",
									numeroFichas : -1
								};
							console.log("Estoy enviando: "+msg1);
							self.ws.send(JSON.stringify(msg1));
							break;
						}
					}
				}
			} else{
				for (var a=0; a<self.fichasJugador().length; a++){
					if (self.fichasJugador()[a].left == data.leftAnt && self.fichasJugador()[a].right == data.rightAnt) {
						self.fichasJugador.splice(a, 1);
					}
				}
				self.mensajeErr("Es el turno del rival");
			}
			if(data.posicionActualizada == "iz"){
				self.fichaIzdaEnMesa(new Ficha(data.leftPuesta, data.rightPuesta));
			}else if(data.posicionActualizada == "dc"){
				self.fichaDchaEnMesa(new Ficha(data.leftPuesta, data.rightPuesta));
			} 
			
			if(self.fichaDchaEnMesa().right == -1){
				self.fichaDchaEnMesa(new Ficha(data.mejorDoble,data.mejorDoble));
			}
			
			if(data.robadaR >= 0 || data.robadaR <= 7){
				if(data.robadaPor == miSession){
					self.fichasJugador.push(new Ficha(data.robadaL, data.robadaR));
				}
			}
			
		} else if (data.type == "error"){
			if(data.mensaje == "Index: 0, Size: 0"){
				self.mensajeErr("Pozo sin fichas");
			}else{
				self.mensajeErr(data.mensaje);
			}
		} else if (data.type == "ganador"){
			
			alert("El ganador de esta partida es: "+ data.ganador +"!");
			window.location.href = "games.html";
		}
	}
	
	self.robar = function(){
		var msg = {
				type : "jugada",
				idMatch : sessionStorage.idMatch,
				subType : "robar"
		};
		self.ws.send(JSON.stringify(msg));
	}
}

class Ficha {
	constructor(left, right) {
		this.left = left;
		this.right = right;
	}
	
	mover() {
		if ((self.fichaIzdaEnMesa().right == this.right && self.fichaIzdaEnMesa().left == this.left) || (self.fichaDchaEnMesa().right == this.right && self.fichaDchaEnMesa().left == this.left)) {
			self.fichaSeleccionada(new Ficha(this.left, this.right));
		} else {
			if (self.fichaSeleccionada().right == -1){
				alert("Selecciona primero una ficha de la mesa");
				return;
			}
			if(self.fichaIzdaEnMesa().right == self.fichaSeleccionada().right && self.fichaIzdaEnMesa().left == self.fichaSeleccionada().left){
				var msg = {
						type : "jugada",
						idMatch : sessionStorage.idMatch,
						left : this.left,
						right : this.right,
						leftMesa : self.fichaSeleccionada().left,
						rightMesa : self.fichaSeleccionada().right,
						pos : "izquierda",
						numeroFichas : contadorFichas()
					};
					self.nfichas(0);
					console.log("Estoy enviando: "+JSON.stringify(msg));
					self.ws.send(JSON.stringify(msg));
			} else if (self.fichaDchaEnMesa().right == self.fichaSeleccionada().right && self.fichaDchaEnMesa().left == self.fichaSeleccionada().left){
				var msg2 = {
						type : "jugada",
						idMatch : sessionStorage.idMatch,
						left : this.left,
						right : this.right,
						leftMesa : self.fichaSeleccionada().left,
						rightMesa : self.fichaSeleccionada().right,
						pos : "derecha",
						numeroFichas : contadorFichas()
							
					};
					self.nfichas(0);
					console.log("Estoy enviando: "+JSON.stringify(msg2));
					self.ws.send(JSON.stringify(msg2));
			}
		}
	}
}

function contadorFichas(){
	for (var i=0; i<self.fichasJugador().length; i++){
		if(self.nfichas() != -1){
			self.nfichas(self.nfichas() + 1);
		}else{
			self.nfichas(0);
		}
	}
	console.log("Este usuario tiene "+self.nfichas()+" fichas");
	return self.nfichas();
}

var vm = new ViewModel();
ko.applyBindings(vm);