package edu.uclm.esi.games2020.ws;

import java.util.List;

import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import edu.uclm.esi.games2020.model.Manager;
import edu.uclm.esi.games2020.model.Match;
import edu.uclm.esi.games2020.model.User;

@Component
public class SpringWebSocket extends TextWebSocketHandler {
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		HttpHeaders headers = session.getHandshakeHeaders();
		List<String> cookies = headers.get("cookie");
		for (String cookie : cookies)
			if (cookie.startsWith("JSESSIONID=")) {
				String httpSessionId = cookie.substring("JSESSIONID=".length());
				User user = Manager.get().findUserByHttpSessionId(httpSessionId);
				user.setSession(session);
				break;
			}
	}
	
	@Override
	public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
		JSONObject jso = new JSONObject(message.getPayload().toString());
		if (jso.getString("type").equals("ready")) {
			Manager.get().playerReady(jso.getString("idMatch"));
		}else if(jso.getString("type").equals("jugada")){
			String idMatch = jso.getString("idMatch");
			Match match = Manager.get().findMatch(idMatch);
			try {
				match.jugada(session, jso);
			}catch(Exception e) {
				jso = new JSONObject();
				jso.put("type", "error");
				jso.put("mensaje",e.getMessage());
				session.sendMessage(new TextMessage(jso.toString()));
			}
		}else {
			jso = new JSONObject();
			jso.put("type", "error");
			jso.put("mensaje", "Mensaje incomprensible");
			session.sendMessage(new TextMessage(jso.toString()));
		}
	}
}
