package edu.uclm.esi.games2020.exceptions;

public class NoCoincidenException extends Exception{
	private static final long serialVersionUID = 1L;
	public NoCoincidenException(){}
    public NoCoincidenException(String mensaje){
        super(mensaje);
    }
}
