package edu.uclm.esi.games2020.exceptions;

public class MovIlegalException extends Exception{
	private static final long serialVersionUID = 1L;
	public MovIlegalException(){}
    public MovIlegalException(String mensaje){
        super(mensaje);
    }
}
