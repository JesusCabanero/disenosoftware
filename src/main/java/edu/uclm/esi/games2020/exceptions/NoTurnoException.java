package edu.uclm.esi.games2020.exceptions;

public class NoTurnoException extends Exception{
	private static final long serialVersionUID = 1L;
	public NoTurnoException(){}
    public NoTurnoException(String mensaje){
        super(mensaje);
    }
}
