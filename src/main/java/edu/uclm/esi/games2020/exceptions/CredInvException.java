package edu.uclm.esi.games2020.exceptions;

public class CredInvException extends Exception{
	private static final long serialVersionUID = 1L;
	public CredInvException(){}
    public CredInvException(String mensaje){
        super(mensaje);
    }
}
