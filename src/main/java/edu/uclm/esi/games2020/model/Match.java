package edu.uclm.esi.games2020.model;

import java.io.IOException;
import edu.uclm.esi.games2020.exceptions.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.socket.WebSocketSession;

public abstract class Match {
	protected List<User> players;
	protected String id;
	protected boolean started;
	protected int readyPlayers;
	protected Game game;
	protected User jugadorCET; //Jugador con el turno
	
	public Match() {
		this.id = UUID.randomUUID().toString();
		this.players = new ArrayList<>();
	}

	public void addPlayer(User user) {
		this.players.add(user);
		setState(user);
	}
	
	protected abstract void setState(User user); 

	public List<User> getPlayers() {
		return players;
	}
	
	public String getId() {
		return id;
	}

	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("idMatch", this.id);
		jso.put("started", this.started);
		jso.put("gameType", this.game.getName());
		JSONArray jsa = new JSONArray();
		for (User user : this.players)
			jsa.put(user.toJSON());
		jso.put("players", jsa);
		return jso;
	}

	public void notifyStart() throws IOException {
		JSONObject jso = this.toJSON();
		jso.put("type", "matchStarted");
		for (User player : this.players) {
			jso.put("startData", startData(player));
			player.send(jso);
		}
	}

	protected abstract JSONObject startData(User player);

	public void playerReady() {
		++readyPlayers;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public boolean ready() {
		return this.readyPlayers==game.requiredPlayers;
	}

	public void jugada(WebSocketSession session, JSONObject jsoMovimiento) throws MovIlegalException, NoTurnoException, IOException {
		if(!tieneTurno(session))
			throw new MovIlegalException("No tienes el turno");
		if(!legal(session,jsoMovimiento))
			throw new NoTurnoException("Movimiento ilegal");
		actualizarTablero(session, jsoMovimiento);
		JSONObject jsoRespuesta = cambiarTurno();
		comprobarGanador(session, jsoMovimiento, jsoRespuesta);
	}

	protected abstract JSONObject cambiarTurno();

	protected abstract boolean legal(WebSocketSession session, JSONObject jso);

	protected abstract void actualizarClientes(WebSocketSession session, JSONObject jso) throws IOException;

	protected abstract void comprobarGanador(WebSocketSession session, JSONObject jso, JSONObject jsoRespuesta) throws IOException;

	protected abstract void actualizarTablero(WebSocketSession session, JSONObject jso);

	protected boolean tieneTurno(WebSocketSession session) throws NoTurnoException {
		WebSocketSession sessionTurno = this.jugadorCET.getSession();
		if(sessionTurno!=session)
			throw new NoTurnoException("No tienes el turno");
		return true;
	}
}