package edu.uclm.esi.games2020.model;

import java.io.IOException;
import java.util.ArrayList;
import org.json.*;
import org.springframework.web.socket.WebSocketSession;
 
public class DominoMatch extends Match {

	private static final String ACTUTABLERO = "actualizacionTablero";
	private static final String ASIGTUR = "asignacionTurno";
	private static final String GANADOR = "ganador";
	private static final String IZQUIERDA = "izquierda";
	private static final String LEFTMESA = "leftMesa";
	private static final String RIGHT = "right";
	private static final String ROBAR = "robar";
	private static final String SUBTYPE = "subType";
	private static final String TURNO = "turno";

	private ArrayList<Counter> countersOnBoard;
	private DominoCounters counters;
	private int mejorDobleJ0 = -1;
	private int mejorDobleJ1 = -1;
	private Counter[] fichasJ0;
	private int nfichasJ0 = -1;
	private int nfichasJ1 = -1;
	private Counter[] fichasJ1;
	private int veces = 0;
	private int giro = 0;

	public DominoMatch() {
		super();
		this.counters = new DominoCounters();
		counters.shuffle();
		this.countersOnBoard = new ArrayList<>();
	}

	@Override
	protected JSONObject startData(User player) {

		JSONObject jso = new JSONObject();
		JSONArray jsaFichasJugador = new JSONArray();

		Counter[] fichas = new Counter[7];

		for (int i = 0; i < fichas.length; i++)
			fichas[i] = this.counters.getCounter();

		for (int j = 0; j < fichas.length; j++)
			fichas[j].setState(player.getState());

		for (int k = 0; k < fichas.length; k++)
			jsaFichasJugador.put(fichas[k].toJSON());

		this.jugadorCET = this.players.get(0);

		if (this.players.get(0).getUserName().equals(player.getUserName())) {
			fichasJ0 = fichas;
		} else {
			fichasJ1 = fichas;
		}

		jso.put("miSession", player.getSession().getId());
		jso.put("data", jsaFichasJugador);
		jso.put(TURNO, this.jugadorCET.getSession().getId());

		return jso;
	}

	protected String asignarTurno() {
		int aux = -1;
		for (int i = 0; i < fichasJ0.length; i++) {
			if ((fichasJ0[i].getRight() == fichasJ0[i].getLeft()) && (fichasJ0[i].getRight() > aux)) {
				aux = fichasJ0[i].getRight();
			}
		}
		mejorDobleJ0 = aux;
		aux = -1;
		for (int i = 0; i < fichasJ1.length; i++) {
			if ((fichasJ1[i].getRight() == fichasJ1[i].getLeft()) && (fichasJ1[i].getRight() > aux)) {
				aux = fichasJ1[i].getRight();
			}
		}
		mejorDobleJ1 = aux;

		if (mejorDobleJ0 != -1 || mejorDobleJ1 != -1) {
			if (mejorDobleJ0 > mejorDobleJ1) {
				this.jugadorCET = this.players.get(0);
				return this.players.get(0).getSession().getId();
			} else {
				this.jugadorCET = this.players.get(1);
				return this.players.get(1).getSession().getId();
			}
		} else {
			this.jugadorCET = this.players.get(0);
			return this.players.get(0).getSession().getId();
		}
	}

	@Override
	protected void setState(User user) {// Preguntar en tutoría para qué va a servir
		IState state = new EscobaState();
		user.setState(state);
		state.setUser(user);
	}

	protected JSONObject cambiarTurno() {
		JSONObject jsoRespuesta = new JSONObject();
		if (veces == 0) {
			jsoRespuesta.put(TURNO, asignarTurno());
			veces++;
		} else {
			if (this.jugadorCET.getSession().equals(this.players.get(0).getSession())) {
				this.jugadorCET = this.players.get(1);
				jsoRespuesta.put(TURNO, this.players.get(1).getSession().getId());
			} else {
				this.jugadorCET = this.players.get(0);
				jsoRespuesta.put(TURNO, this.players.get(0).getSession().getId());
			}
			veces++;
		}
		return jsoRespuesta;
	}

	@Override
	protected void actualizarClientes(WebSocketSession session, JSONObject jsoRespuesta) throws IOException {
		for (User player : this.players) {
			player.send(jsoRespuesta);
		}
	}

	@Override
	protected void comprobarGanador(WebSocketSession session, JSONObject jso, JSONObject jsoRespuesta)
			throws IOException {
		if (jso.has(SUBTYPE)) {
			jsoRespuesta = subtype(jso.getString(SUBTYPE),session,jsoRespuesta);
		} else {
			if (nfichasJ0 != -1 && nfichasJ1 != -1) {
				jsoRespuesta = ganador(jsoRespuesta);
				
			}
			if (!jsoRespuesta.has(GANADOR)) {
				jsoRespuesta = noGanador(jso,jsoRespuesta);
			}
		} 
		try {
			actualizarClientes(session, jsoRespuesta);
		} catch (IOException e) {
			throw new IOException("No se han podido actualizar los clientes.");
		}
	}

	protected JSONObject noGanador(JSONObject jso, JSONObject jsoRespuesta) {
		JSONObject jres = jsoRespuesta;
		jres.put("type", ACTUTABLERO);
		if (jso.getInt(LEFTMESA) == -1) {
			jres.put("posicionActualizada", "iz");
		} else {
			jres.put("posicionActualizada", (jso.getString("pos").equals(IZQUIERDA)) ? "iz" : "dc");
		}
		if (jso.getString("pos").equals(IZQUIERDA)) {
			jres.put("leftPuesta", this.countersOnBoard.get(0).getLeft());
			jres.put("rightPuesta", this.countersOnBoard.get(0).getRight());
		} else {
			jres.put("leftPuesta", this.countersOnBoard.get(1).getLeft());
			jres.put("rightPuesta", this.countersOnBoard.get(1).getRight());
		}
		jres.put("leftAnt", jso.getInt("left"));
		jres.put("rightAnt", jso.getInt(RIGHT));
		return jres;
	}

	protected JSONObject ganador(JSONObject jsoRespuesta) {
		JSONObject jres = jsoRespuesta;
		if (nfichasJ0 <= 1) {
			jres.put(GANADOR, this.players.get(0).getUserName());
			jres.put("type", GANADOR);
		} else if (nfichasJ1 <= 1) {
			jres.put(GANADOR, this.players.get(1).getUserName());
			jres.put("type", GANADOR);
		}
		return jres;
	}

	protected JSONObject subtype(String subty,WebSocketSession session, JSONObject jsoRespuesta) {
		JSONObject jres = jsoRespuesta;
		if (subty.equals(ASIGTUR)) {
			jres.put("type", ACTUTABLERO);
			jres.put("mejorDoble", mejorDobleJ0 > mejorDobleJ1 ? mejorDobleJ0 : mejorDobleJ1);
		} else if (subty.equals(ROBAR)) {
			jres.put("type", ACTUTABLERO);
			Counter robada = this.counters.getCounter();
			jres.put("robadaR", robada.getRight());
			jres.put("robadaL", robada.getLeft());
			jres.put("robadaPor", session.getId());
		}
		return jres;
	}

	@Override
	protected boolean legal(WebSocketSession session, JSONObject jso) {
		if (jso.has(SUBTYPE)) {
			return true;
		} else {
			if (jso.getInt(LEFTMESA) == -1) {
				// Se pone la primera ficha que va a ser el mayor doble porque
				// ha empezado la partida y no hay fichas en la mesa por lo que siempre va a ser
				// legal
				return true;
			} else {
				giro = coincide(jso.getString("pos"), jso.getInt("left"), jso.getInt(LEFTMESA), jso.getInt(RIGHT),
						jso.getInt("rightMesa"));
				if ((giro == 1) || (giro == 2) || (giro == 3) || (giro == 4)) {
					return true;
				}
			}
		}
		return false;
	}

	protected int coincide(String pos, int left, int leftmesa, int right, int rightmesa) {
		int gi = 0;
		if (pos.equals(IZQUIERDA))
			gi = coincideIz(left, right, leftmesa);
		else if (pos.equals("derecha"))
			gi = coincideDer(left, right, rightmesa);
		return gi;
	}

	private int coincideDer(int left, int right, int rightmesa) {
		int gi1 = 0;
		if (left == rightmesa) {
			gi1 = 3; // Ha coincidido por la izquierda con la ficha de la derecha
		} else if (right == rightmesa) {
			gi1 = 4; // Ha coincidido por la derecha con la ficha de la derecha
		}
		return gi1;
	}

	protected int coincideIz(int left, int right, int leftmesa) {
		int gi1 = 0;
		if (left == leftmesa) {
			gi1 = 1; // Ha coincidido por la izquierda con la ficha de la izquierda
		} else if (right == leftmesa) {
			gi1 = 2; // Ha coincidido por la derecha con la ficha de la izquierda
		}
		return gi1;
	}

	@Override
	protected void actualizarTablero(WebSocketSession session, JSONObject jso) {
		if (jso.has(SUBTYPE)) {
			// Saltar sin hacer nada, se está robando ficha o se está asignando el turno
		} else if (jso.getString("type").equals("jugada")) {
			if (jso.getInt(LEFTMESA) == -1) {
				int fichaInicial = jso.getInt("left");
				this.countersOnBoard.add(0, new Counter(fichaInicial, fichaInicial)); // Se añadirá en la posición 0 que
																						// es
				this.countersOnBoard.add(1, new Counter(fichaInicial, fichaInicial)); // la izquierda
			} else {
				switch (giro) {
				case 1:
					this.countersOnBoard.remove(0);
					this.countersOnBoard.add(0, new Counter(jso.getInt(RIGHT), jso.getInt("left")));
					break;
				case 2:
					this.countersOnBoard.remove(0);
					this.countersOnBoard.add(0, new Counter(jso.getInt("left"), jso.getInt(RIGHT)));
					break;
				case 3:
					this.countersOnBoard.remove(1);
					this.countersOnBoard.add(1, new Counter(jso.getInt("left"), jso.getInt(RIGHT)));
					break;
				case 4:
					this.countersOnBoard.remove(1);
					this.countersOnBoard.add(1, new Counter(jso.getInt(RIGHT), jso.getInt("left")));
					break;
				default:
					// Siempre va a haber una forma en la que va a encajar porque si no no habría
					// llegado hasta aquí
					break;
				}
				giro = 0;
			}
			if (this.players.get(0).getSession().getId().equals(session.getId())) {
				nfichasJ0 = jso.getInt("numeroFichas");
			} else {
				nfichasJ1 = jso.getInt("numeroFichas");
			}
		}
	}
}
