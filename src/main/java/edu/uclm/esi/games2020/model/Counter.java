package edu.uclm.esi.games2020.model;

import org.json.JSONObject;

public class Counter {
	private int right;
	private int left;
	private IState state;

	public Counter(int left, int right) {
		super();
		this.right = right;
		this.left = left;
	}
	
	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("right", this.right);
		jso.put("left", this.left);
		return jso;
	}
	
	public void setState(IState state) {
		this.state = state;
	}
	
	public IState getState() {
		return state;
	}
	
	@Override
	public String toString() {
		return this.right+" : "+ this.left;
	}

	public int getRight() {
		return right;
	}

	public void setRight(int right) {
		this.right = right;
	}

	public int getLeft() {
		return left;
	}

	public void setLeft(int left) {
		this.left = left;
	}
}
