package edu.uclm.esi.games2020.model;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

public class Utilidades {
	
	private static final String DESede = "DESede";
	private static final String UTF8 = "utf-8";

    public static String encriptar(String texto) {

        String secretKey = "disenosoftware"; //clave de encriptación
        String base64EncryptedString = "";

        try {

            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] digestOfPassword = md.digest(secretKey.getBytes(UTF8));
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);

            SecretKey key = new SecretKeySpec(keyBytes, DESede);
            Cipher cipher = Cipher.getInstance(DESede);
            cipher.init(Cipher.ENCRYPT_MODE, key);

            byte[] plainTextBytes = texto.getBytes(UTF8);
            byte[] buf = cipher.doFinal(plainTextBytes);
            byte[] base64Bytes = Base64.encodeBase64(buf);
            base64EncryptedString = new String(base64Bytes);

        } catch (Exception ex) {
        	//No se implementa porque no hay posibilidad de error
        }
        return base64EncryptedString;
    }

    public static String desencriptar(String textoEncriptado) {

        String secretKey = "disenosoftware"; //clave de desencriptación
        String base64EncryptedString = "";

        try {
            byte[] message = Base64.decodeBase64(textoEncriptado.getBytes(UTF8));
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] digestOfPassword = md.digest(secretKey.getBytes(UTF8));
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
            SecretKey key = new SecretKeySpec(keyBytes, DESede);

            Cipher decipher = Cipher.getInstance(DESede);
            decipher.init(Cipher.DECRYPT_MODE, key);

            byte[] plainText = decipher.doFinal(message);

            base64EncryptedString = new String(plainText, StandardCharsets.UTF_8);

        } catch (Exception ex) {
        	//No se implementa porque no hay posibilidad de fallo
        }
        return base64EncryptedString;
    }
}