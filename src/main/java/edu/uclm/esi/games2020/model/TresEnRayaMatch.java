package edu.uclm.esi.games2020.model;

import java.io.IOException;
import org.json.*;
import org.springframework.web.socket.WebSocketSession;

public class TresEnRayaMatch extends Match {

	private static final String GANADOR = "ganador";
	private static final String POSICION = "posicion";
	private static final String TURNO = "turno";
	
	private int[] tablero;

	public TresEnRayaMatch() {
		super();
		this.tablero = new int[9];
	}

	public int[] getTablero() {
		return tablero;
	}

	public void setTablero(int[] tablero) {
		this.tablero = tablero;
	}

	@Override
	protected void setState(User user) {
		IState state = new EscobaState();
		user.setState(state);
		state.setUser(user);
	}

	@Override
	protected JSONObject startData(User player) {
		this.jugadorCET = this.players.get(0);
		JSONObject jso = new JSONObject();
		JSONArray jsa = new JSONArray();
		jsa.put(getTablero());
		jso.put("tablero", jsa);
		jso.put(TURNO, jugadorCET.getSession().getId());
		jso.put("miSession", player.getSession().getId());
		if(jugadorCET.getSession().equals(player.getSession())) {
			jso.put("ficha", 1);
		}else {
			jso.put("ficha", 2);
		}
		return jso;
	}

	@Override
	protected boolean legal(WebSocketSession session, JSONObject jso) {
		int posicion = jso.getInt(POSICION);
		return this.tablero[posicion] == 0;
	}
	
	@Override
	protected void actualizarClientes(WebSocketSession session, JSONObject jsoRespuesta) throws IOException {
		for (User player : this.players) {
			player.send(jsoRespuesta);
		}
	}

	@Override
	protected void comprobarGanador(WebSocketSession session, JSONObject jsoMovimiento, JSONObject jsoRespuesta) throws IOException {
		int posicion = jsoMovimiento.getInt(POSICION);
		int ficha = this.players.get(0).getSession()==session ? 1 : 2;
		if(comprobacionVertical(ficha) || comprobacionHorizontal(ficha) || comprobacionDiagonal(ficha)) {
			if(this.players.get(0).getSession().getId().equals(session.getId()))
				jsoRespuesta.put(GANADOR, this.players.get(0).getUserName());
			else
				jsoRespuesta.put(GANADOR, this.players.get(1).getUserName());
			jsoRespuesta.put("type", GANADOR);
			jsoRespuesta.put("fichaFinal", posicion);
			jsoRespuesta.put("valorFinal", this.tablero[posicion]);
		} else {
			int em = 0;
			for(int i = 0; i < this.tablero.length; i++) {
				em = em + this.tablero[i];
			}
			if(em == 13) {
				jsoRespuesta.put("empate", "Empate");
			}
			jsoRespuesta.put("type", "actualizacionTablero");
			jsoRespuesta.put("posicionActualizada", posicion);
			jsoRespuesta.put("fichaPuesta", this.tablero[posicion]);
		}
		try {
			actualizarClientes(session, jsoRespuesta);
		}catch(Exception e) {
			throw new IOException("No se han podido actualizar los clientes.");
		}
	}

	private boolean comprobacionVertical(int ficha) {
		return ((this.tablero[0] == ficha && this.tablero[3] == ficha && this.tablero[6] == ficha) || (this.tablero[1] == ficha && this.tablero[4] == ficha && this.tablero[7] == ficha) || (this.tablero[2] == ficha && this.tablero[5] == ficha && this.tablero[8] == ficha));
	}
	
	private boolean comprobacionHorizontal(int ficha) {
		return ((this.tablero[3] == ficha && this.tablero[4] == ficha && this.tablero[5] == ficha) || (this.tablero[0] == ficha && this.tablero[1] == ficha && this.tablero[2] == ficha) || (this.tablero[6] == ficha && this.tablero[7] == ficha && this.tablero[8] == ficha));
	}
	
	private boolean comprobacionDiagonal(int ficha) {
		return ((this.tablero[0] == ficha && this.tablero[4] == ficha && this.tablero[8] == ficha) || (this.tablero[6] == ficha && this.tablero[2] == ficha && this.tablero[4] == ficha));
	}

	@Override
	protected void actualizarTablero(WebSocketSession session, JSONObject jso) {
		int ficha = this.players.get(0).getSession()==session ? 1 : 2;
		int posicion = jso.getInt(POSICION);
		this.tablero[posicion] = ficha;
	}

	@Override
	protected JSONObject cambiarTurno() {
		JSONObject jsoRespuesta = new JSONObject();
		if(this.jugadorCET.getSession().equals(this.players.get(0).getSession())) {
			this.jugadorCET=this.players.get(1);
			jsoRespuesta.put(TURNO, this.players.get(1).getSession().getId());
		}else {
			this.jugadorCET=this.players.get(0);
			jsoRespuesta.put(TURNO, this.players.get(0).getSession().getId());
		}
		return jsoRespuesta;
	}
}
