package edu.uclm.esi.games2020.model;

import java.util.List;
import java.security.SecureRandom;
import java.util.ArrayList;

public class DominoCounters {
	private List<Counter> counters;

	public DominoCounters() {
		int rotador = 6;
		this.counters = new ArrayList<>();
		while (rotador >= 0) {
			for (int i = 0; i <= rotador; i++) {
				Counter counter = new Counter(rotador, i);
				this.counters.add(counter);
			}
			rotador--;
		}
	}
	
	public void shuffle() {
		SecureRandom dado = new SecureRandom();
		for (int i=0; i<200; i++) {
			int a = dado.nextInt(28); //Escoge dos fichas al azar dentro
			int b = dado.nextInt(28); //de las 28 que se han generado
			Counter auxiliar = this.counters.get(a); 
			this.counters.set(a, this.counters.get(b)); //Barajea las fichas
			this.counters.set(b, auxiliar); //sustituyendo sus posiciones
		}
	}
	
	public Counter getCounter() {
		return this.counters.remove(0);
	}
}
